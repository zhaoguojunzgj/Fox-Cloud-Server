package cn.foxtech.cloud.manager.repository.constants;

public class ConstantRepoGroup extends Constant {
    public static final String field_collection_name = "edgeRepoGroup";

    public static final String field_group_name = "groupName";

    public static final String field_member = "member";

    public static final String field_group_description = "description";
}
