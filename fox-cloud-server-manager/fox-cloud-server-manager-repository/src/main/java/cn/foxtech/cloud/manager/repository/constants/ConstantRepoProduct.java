package cn.foxtech.cloud.manager.repository.constants;

public class ConstantRepoProduct extends Constant {
    public static final String field_collection_name = "edgeRepoProduct";

    public static final String field_uuid = "uuid";

    public static final String field_model = "model";

    public static final String field_manufacturer = "manufacturer";

    public static final String field_image = "image";

    public static final String field_url = "url";

    public static final String field_tags = "tags";

    public static final String field_description = "description";

    public static final String field_comps = "comps";

    public static final String field_weight = "weight";

    public static final String field_key_word = "keyWord";
}
