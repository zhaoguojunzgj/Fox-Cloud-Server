package cn.foxtech.cloud.manager.system.constants;

public class Constant {
    public static final String field_entity_type = "entityType";
    public static final String field_edge_id = "edgeId";

    public static final String field_page_num = "pageNum";
    public static final String field_page_size = "pageSize";

}
