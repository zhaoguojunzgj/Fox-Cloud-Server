package cn.foxtech.cloud.common.mongo.constants;

public class EdgeServerConstant {
    /**
     * Fox-Edge的设备信息
     */
    public static final String field_table_name = "edgeServer";
    public static final String field_index_name = "name";
    public static final String field_index_edge_id = "edgeId";
    public static final String field_redis_key = "fox.cloud.edge.EdgeServer";
}
